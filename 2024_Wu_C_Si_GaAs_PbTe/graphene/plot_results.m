clear;font_size=15;
load energy_test.out;load virial_test.out;load force_test.out; load loss.out; 
loss_end=loss(end,8:10)
n=100:100:200000;
figure;
subplot(2, 2, 1);
plot(force_test(:,4:6),force_test(:,1:3),'.','markersize',10); hold on;
plot(-20:0.01:20,-20:0.01:20,'k--','linewidth',1);
xlabel('DFT force (eV/$\AA$)','fontsize',15,'interpreter','latex');
ylabel('NEP force (eV/\rm $\AA$)','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
text(-7.5,7.5,    num2str(loss_end(:,2)) ,'fontsize',15,'interpreter','latex')
%text(-5.5,7.5,  'eV/$\AA$','fontsize',15,'interpreter','latex')
axis tight;
title('(a)');
subplot(2, 2, 2);
plot(energy_test(:,2),energy_test(:,1),'.','color',[0.8 0.2 0],'markersize',10); hold on;
plot(-7.1:0.01:-5.6,-7.1:0.01:-5.6,'k--','linewidth',1);
xlabel('DFT energy (eV/atom)','fontsize',15,'interpreter','latex');
ylabel('NEP energy (eV/atom)','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
text(-6.6,-5.8, num2str(loss_end(:,1)),'fontsize',font_size,'interpreter','latex')
%text(-4.6,-5.8,  'eV/atom','fontsize',font_size,'interpreter','latex')
axis tight;
title('(b)');
subplot(2, 2, 3);
plot(virial_test(:,2),virial_test(:,1),'.','color',[0.2 0.8 0],'markersize',10); hold on;
plot(-3:0.01:5,-3:0.01:5,'k--','linewidth',1);
xlabel('DFT virial (eV/atom)','fontsize',15,'interpreter','latex');
ylabel('NEP virial (eV/atom)','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
text(-1.68,3.5, num2str(loss_end(:,3)),'fontsize',font_size,'interpreter','latex')
%text(-1.48,3.5, 'eV/atom','fontsize',font_size,'interpreter','latex')
axis tight;
title('(c)');

subplot(2, 2, 4);
loglog(n,loss(:,8:10),'-','linewidth',2); hold on;
%loglog(loss(:,1),loss(:,8:9),'-','linewidth',4); hold on;
xlabel('Generation','fontsize',15,'interpreter','latex');
ylabel('Loss functions','fontsize',15,'interpreter','latex');;
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
legend('Energy-test','Force-test','Stress-test');
axis tight
title('(d)');
