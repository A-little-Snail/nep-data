#!/bin/bash
#SBATCH -J PbTe 		#作业名称
#SBATCH -p gpu --gres=gpu:4
#SBATCH -n 1			#作业使用CPU核数
#SBATCH --error=%J.err 		#作业错误输出
#SBATCH --output=%J.out 	#作业日志输出
## run GPUMD ##
module load cuda/11.7.1-gcc-9.4.0
module load gcc/9.4.0-gcc-4.8.5
ulimit -s unlimited
/public/home/syxiong_cnxy/software/GPUMD-3.8/src/nep < nep.in > output
scontrol show job $SLURM_JOBID  #作业信息输出

