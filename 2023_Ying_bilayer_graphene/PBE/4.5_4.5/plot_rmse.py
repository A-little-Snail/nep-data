from pylab import *

##set figure properties
aw = 1.5
fs = 16
lw = 1.5
font = {'size'   : fs}
matplotlib.rc('font', **font)
matplotlib.rc('axes' , lw=aw)

def set_fig_properties(ax_list):
    tl = 6
    tw = 1.5
    tlm = 3
    
    for ax in ax_list:
        ax.tick_params(which='major', length=tl, width=tw)
        ax.tick_params(which='minor', length=tlm, width=tw)
        ax.tick_params(which='both', axis='both', direction='out', right=False, top=False)


loss = loadtxt('./loss.out')
loss[:,0] = np.arange(1, len(loss) + 1)*100
print("We have run %s steps!"%loss[-1, 0])
energy_train = loadtxt('./energy_train.out')
force_train = loadtxt('./force_train.out')
virial_train = loadtxt('./virial_train.out')

figure(figsize=(18, 5))
subplot(1, 3, 1)
set_fig_properties([gca()])
plot(energy_train[:, 1], energy_train[:, 0], 'o', c="C2", ms = 5, alpha=0.5, label="Train")
plot([np.min(energy_train)-0.1, np.max(energy_train)+0.15], [np.min(energy_train)-0.1, np.max(energy_train)+0.15], c = "grey", lw = 1)
xlim([np.min(energy_train)-0.1, np.max(energy_train)+0.15])
ylim([np.min(energy_train)-0.1, np.max(energy_train)+0.15])
gca().set_xticks(linspace(-9.2, -8.0, 5))
gca().set_yticks(linspace(-9.2, -8.0, 5))
xlabel('DFT energy (eV/atom)')
ylabel('NEP energy (eV/atom)')
title("(a)")


subplot(1, 3, 2)
set_fig_properties([gca()])
plot(force_train[:, 3], force_train[:, 0], 'o', c="C3", ms = 5, alpha=0.5, label="Train")
plot(force_train[:, 4:6], force_train[:, 1:3], 'o', c="C3", ms = 5, alpha=0.5)
plot([np.min(force_train)-1, np.max(force_train)+1], [np.min(force_train)-1, np.max(force_train)+1], c = "grey", lw = 1)
xlim([np.min(force_train)-1, np.max(force_train)+1])
ylim([np.min(force_train)-1, np.max(force_train)+1])
gca().set_xticks(linspace(-20, 20, 5))
gca().set_yticks(linspace(-20, 20, 5))
xlabel(r'DFT force (eV/$\rm{\AA}$)')
ylabel(r'NEP force (eV/$\rm{\AA}$)')
title("(b)")


subplot(1, 3, 3)
set_fig_properties([gca()])
plot(virial_train[:, 6], virial_train[:, 0], 'o', c="C4", ms = 5, alpha=0.5, label="Train")
plot(virial_train[:, 7:12], virial_train[:, 1:6], 'o', c="C4", ms = 5, alpha=0.5)
plot([np.min(virial_train)-1, np.max(virial_train)+1], [np.min(virial_train)-1, np.max(virial_train)+1], c = "grey", lw = 1)
xlim([np.min(virial_train)-1, np.max(virial_train)+1])
ylim([np.min(virial_train)-1, np.max(virial_train)+1])
gca().set_xticks(linspace(-2.5, 5, 4))
gca().set_yticks(linspace(-2.5, 5, 4))
xlabel('DFT virial (eV/atom)')
ylabel('NEP virial (eV/atom)')
title("(c)")

subplots_adjust(wspace=0.35, hspace=0.3)
savefig("RMSE.pdf", bbox_inches='tight')
