from pylab import *

##set figure properties
aw = 1.5
fs = 16
lw = 1.5
font = {'size'   : fs}
matplotlib.rc('font', **font)
matplotlib.rc('axes' , lw=aw)

def set_fig_properties(ax_list):
    tl = 6
    tw = 1.5
    tlm = 3
    
    for ax in ax_list:
        ax.tick_params(which='major', length=tl, width=tw)
        ax.tick_params(which='minor', length=tlm, width=tw)
        ax.tick_params(which='both', axis='both', direction='out', right=False, top=False)



figure(figsize=(16, 8))
e_3   = np.loadtxt("3_3/energy_train.out")
e_3p5 = np.loadtxt("3.5_3.5/energy_train.out")
e_4   = np.loadtxt("4_4/energy_train.out")
e_4p5 = np.loadtxt("Selected_4.5_4.5/energy_train.out")
e_5   = np.loadtxt("5_5/energy_train.out")
subplot(1, 3, 1)
set_fig_properties([gca()])
plot(e_3[:, 1],   abs(e_3[:, 0]-e_3[:,1]),   'o', c="C0", ms = 5, alpha=0.5, label=r"$3.0 \mathrm{\AA}$")
plot(e_3p5[:, 1], abs(e_3p5[:, 0]-e_3[:,1]), 'o', c="C1", ms = 5, alpha=0.5, label=r"$3.5 \mathrm{\AA}$")
plot(e_4[:, 1],   abs(e_4[:, 0]-e_3[:,1]),   'o', c="C2", ms = 5, alpha=0.5, label=r"$4.0 \mathrm{\AA}$")
plot(e_4p5[:, 1], abs(e_4p5[:, 0]-e_3[:,1]), 'o', c="C3", ms = 5, alpha=0.5, label=r"$4.5 \mathrm{\AA}$")
plot(e_5[:, 1],   abs(e_5[:, 0]-e_3[:,1]),   'o', c="C4", ms = 5, alpha=0.5, label=r"$5.0 \mathrm{\AA}$")
xlabel(r'$E_{\mathrm{DFT}}$ (eV/atom)')
ylabel(r'$|E_{\mathrm{NEP}} - E_{\mathrm{DFT}}|$ (eV/atom)')
legend(loc="upper right")
title("(a)")

f_3   = np.loadtxt("3_3/force_train.out")
f_3p5 = np.loadtxt("3.5_3.5/force_train.out")
f_4   = np.loadtxt("4_4/force_train.out")
f_4p5 = np.loadtxt("Selected_4.5_4.5/force_train.out")
f_5   = np.loadtxt("5_5/force_train.out")
subplot(1, 3, 2)
set_fig_properties([gca()])
plot(np.concatenate(f_3[:, 3:]),   abs(np.concatenate(f_3[:, :3]) - np.concatenate(f_3[:, 3:])),     'o', c="C0", ms=5, alpha=0.5, label=r"3.0 $\mathrm{\AA}$")
plot(np.concatenate(f_3p5[:, 3:]), abs(np.concatenate(f_3p5[:, :3]) - np.concatenate(f_3p5[:, 3:])), 'o', c="C1", ms=5, alpha=0.5, label=r"3.5 $\mathrm{\AA}$")
plot(np.concatenate(f_4[:, 3:]),   abs(np.concatenate(f_4[:, :3]) - np.concatenate(f_4[:, 3:])),     'o', c="C2", ms=5, alpha=0.5, label=r"4.0 $\mathrm{\AA}$")
plot(np.concatenate(f_4p5[:, 3:]), abs(np.concatenate(f_4p5[:, :3]) - np.concatenate(f_4p5[:, 3:])), 'o', c="C3", ms=5, alpha=0.5, label=r"4.5 $\mathrm{\AA}$")
plot(np.concatenate(f_5[:, 3:]),   abs(np.concatenate(f_5[:, :3]) - np.concatenate(f_5[:, 3:])),     'o', c="C4", ms=5, alpha=0.5, label=r"5.0 $\mathrm{\AA}$")
xlabel(r'$f_{\mathrm{DFT}}$ (eV/$\rm{\AA}$)')
ylabel(r'$|f_{\mathrm{NEP}} - f_{\mathrm{DFT}}|$ (eV/$\rm{\AA}$)')
title("(b)")

v_3   = np.loadtxt("3_3/virial_train.out")
v_3p5 = np.loadtxt("3.5_3.5/virial_train.out")
v_4   = np.loadtxt("4_4/virial_train.out")
v_4p5 = np.loadtxt("Selected_4.5_4.5/virial_train.out")
v_5   = np.loadtxt("5_5/virial_train.out")
subplot(1, 3, 3)
set_fig_properties([gca()])
plot(np.concatenate(v_3[:, 6:]),   abs(np.concatenate(v_3[:, :6]) - np.concatenate(v_3[:, 6:])),     'o', c="C0", ms=5, alpha=0.5, label=r"3.0 $\mathrm{\AA}$")
plot(np.concatenate(v_3p5[:, 6:]), abs(np.concatenate(v_3p5[:, :6]) - np.concatenate(v_3p5[:, 6:])), 'o', c="C1", ms=5, alpha=0.5, label=r"3.5 $\mathrm{\AA}$")
plot(np.concatenate(v_4[:, 6:]),   abs(np.concatenate(v_4[:, :6]) - np.concatenate(v_4[:, 6:])),     'o', c="C2", ms=5, alpha=0.5, label=r"4.0 $\mathrm{\AA}$")
plot(np.concatenate(v_4p5[:, 6:]), abs(np.concatenate(v_4p5[:, :6]) - np.concatenate(v_4p5[:, 6:])), 'o', c="C3", ms=5, alpha=0.5, label=r"4.5 $\mathrm{\AA}$")
plot(np.concatenate(v_5[:, 6:]),   abs(np.concatenate(v_5[:, :6]) - np.concatenate(v_5[:, 6:])),     'o', c="C4", ms=5, alpha=0.5, label=r"5.0 $\mathrm{\AA}$")
xlabel(r'$v_{\mathrm{DFT}}$ (eV/atom)')
ylabel(r'$|v_{\mathrm{NEP}} - v_{\mathrm{DFT}}|$ (eV/atom)')
title("(c)")

subplots_adjust(wspace=0.3)
savefig("NEP_RMSE.png", bbox_inches='tight')
