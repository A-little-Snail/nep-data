clear; close all;
load energy_train.out;load force_train.out; load loss.out; 

rmse_energy=sqrt(mean((energy_train(:,1)-energy_train(:,2)).^2))
force_diff=reshape(force_train(:,4:6)-force_train(:,1:3),size(force_train,1)*3,1);
rmse_force=sqrt(mean(force_diff.^2))


figure;
subplot(2,2,1);
loglog(loss(:,3:6),'-','linewidth',2); hold on;
xlabel('Generation/100','fontsize',15,'interpreter','latex');
ylabel('Loss functions','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
legend('L1-Reg','L2-Reg','Energy-train','Force-train');
axis tight

subplot(2,2,2);
plot(energy_train(:,2),energy_train(:,1),'.','markersize',20); hold on;
plot(-3.9:0.01:-3.6,-3.9:0.01:-3.6,'linewidth',2);
xlabel('DFT energy (eV/atom)','fontsize',15,'interpreter','latex');
ylabel('NEP energy (eV/atom)','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
title(['RMSE = ',num2str(rmse_energy*1000,'%.1f'),' meV/atom']);
axis tight;

subplot(2,2,3);
plot(force_train(:,4:6),force_train(:,1:3),'.','markersize',20); hold on;
plot(-4:0.01:4,-4:0.01:4,'linewidth',2);
xlabel('DFT force (eV/$\AA$)','fontsize',15,'interpreter','latex');
ylabel('NEP force (eV/$\AA$)','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
title(['RMSE = ',num2str(rmse_force*1000,'%.0f'),' meV/A']);
axis tight;

